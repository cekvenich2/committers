## Docs
- http://docs.AXthe.net

AXthe is an opinionated JAMstack based tech stack for high productivity web app development.

### Please star our repo
- http://github.com/axthe/axthe

### Support and Forum
- http://github.com/axthe/axthe/discussions

### Prerequisite
Prerequisite knowledge ahead of AXthe tech stack is this book (or a similar book) - 'HTML and CSS: Design and Build web sites by Jon Duckett'

# Overview
AXthe tech stack (and 'best' practices for high productivity web app) offer more than one development style, and allows you to combine them:
1. A classic full stack development(based on JAMstack), with 11ty build and Micro frontends(similar to microservices) with native web components, that is compatible with Cordova cross platform development for Mobile(iOS and Android) from a single code base. It uses caddyserver for static, and js for API.
2. In addition to using  for API, you can also server dynamically generated pug pages, and combine them via Caddyfile matcher.
AXthe curates a minimum viable tech stack as: 11ty, FicusJS, CaddyServer, Pug, Bootstrap, , Supabase, Cordova.
Other parts of the tech stack:
- For a modern http server we use Caddy instead of Apache or NGINX, as it is much easier to use, we show you how to set it up behind a DNS for auto-https. All the apps are normally behind Caddy or behind a CDN.
- For IDE we use a cloud based IDE: http://codeanywhere.com IDE (not their containers, you can use http://vultr.com for your host, others are listed in the setup section) or VS Code w/ the SSH plugin ( as described in the setup section)
- For DB we use Supabase, an enhanced hosting service for PostgreSQL.
- While micro front architecture can leverage most .js libraries(eg Svelte, Vue, React ), we use FicusJS ( http://docs.ficusjs.org )

##  For the impatient
If you are already familiar with AXthe, you can download a starter app/kit, that includes the Caddyfile to setup the http server and includes both environments:
- For latest: http://github.com/axthe/axthe and click Code/Download ZIP), or use wget from shell to download the file. ```wget https://github.com/axthe/axthe/archive/refs/heads/main.zip ```
[![asciicast](https://asciinema.org/a/eVl09pd9zi2fCzjxA16MviMmN.svg)](https://asciinema.org/a/eVl09pd9zi2fCzjxA16MviMmN)
In root of the extracted files run caddy, in  folder rename file _env to ```.env```. Running ```dev.sh``` should start your appi app; last in caddy run ```rdev.sh```.

## Directory structure
Pug is templating you already know how to use, it is html markup tags that are self closing! It is a fine alternative to PHP.
Check out pug: http://html2pug.vercel.app. Pug is similar to Haml or Slim, if you have not used it yet, you'll love it. Like you use scss files to generate css (with tools like PrePros.io)
AXthe has a convention of always(with some exceptions) naming files as index.pug. That means if you have 3 pages in your web app, you need 3 folders, you would do this in the caddy folder and build via 11ty.
 
```
   - ./src/lading/A/index.pug
   - ./src/lading/B/index.pug
   - ./src/lading/C/index.pug
```
 
Above convention create 3 landing pages alternatives (suitable for testing of marketing variations):
 
```
   - http://localhost/lading/a
   - http://localhost/lading/b
   - http://localhost/lading/c
```
 
### A more concepts:
- AXthe aims to be the most productive way to build any web app, including mobile or CMS web apps. Specifically it aims to be better than the old productivity leader, LAMP, or even 'low code' tools. It make development fun, and productivity is fast, so that you can build rapid prototypes.
- AXthe tech stack approach is to eschew client side .js, and rely heavily on . ( I really like the word eschew). The server side route in .js intercepts the http request and passes any dynamic data to pug. That is 90% of AXthe. In addition to productivity,  AXthe is better at SEO and security.
- AXthe is a designer friendly tech stack, so it is not just for front end and full stack developers. Even civilians, people not trained in software development can write professional grade web apps.
- AXthe stack also has AXthe npm module that has a few helper classes
 
