
'use strict';

const { v4: uuidv4 } = require('uuid')
const Minio = require('minio')

module.exports =  class MinS3 {

	_mclient 
	_bucketName
	
constructor(bucketName, props ) {
	this._bucketName = bucketName
	this._mclient = new Minio.Client(props)
}//()

/**
 * returns an array
 */
find(prefix){
	const stream = this._mclient.extensions.listObjectsV2WithMetadata(this._bucketName, prefix, true)
	let arr = []
	return new Promise((resolve, reject) => {
		stream.on('data', async (obj)=> { 
			arr.push(obj.name)
		} )
		stream.on('end', () =>{
			resolve(arr)
		})
		stream.on('error', function(err) { 
			console.log('err', err) 
			reject(err)
		} )
	})//pro
}//()

putMap(prefix, obj) {
	const str = JSON.stringify(obj)
	let k = uuidv4()
	let key = k.replace(/-/g,'')
	//console.log('k', key)
	const meta = {'z':'3'}
	this._mclient.putObject(this._bucketName, prefix+'/'+key, str, meta, meta, (err)=>{
		console.log('err', err)
	})
	return key
}

getMap(prefixPlusKey) {
	return new Promise((resolve, reject) => {
		this._mclient.getObject(this._bucketName, prefixPlusKey, (err, dataStream) =>{
			const chunks = []
			if (err) {
				reject(err)
			}
			dataStream.on('data', (chunk) =>{
				chunks.push(chunk) 
			})
			dataStream.on('end', () =>{
				console.log('End:' )
				const str = Buffer.concat(chunks).toString()
				const res = JSON.parse(str)
				resolve(res)
			})
			dataStream.on('error', function(err) {
				reject(err)
			})
		})
	})//pro
}

putTags(){}

getTags(){}

getMeta(prefixPlusKey){}

remove(prefixPlusKey) {}

}//class