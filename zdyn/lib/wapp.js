const EWApp  = require('axthe/EWApp.js')
const APreRouter = require('./APreRouter.js') // this is what gets edited

// ////////////////////////////////////////
const wapp = new EWApp() // make an app

wapp.eapp.use(function (req, res, next) {
	res.set('Cache-control', 'public, max-age=2')
	res.set('Cache-control', 'public, s-maxage=1')
	next()
})
// enable reload on assets/**/style.scss, maybe you don't want this in production:

// now you can add routes
new APreRouter(wapp.eapp) // tell the router about the app

wapp.finalPrep() // listen on this port

