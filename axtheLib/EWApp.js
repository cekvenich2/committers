
'use strict';
const debug = require('debug')
if (debug.enabled) console.log('debug enabled')
const fs = require('fs-extra')
try {
	fs.readFileSync('.env', 'utf8')
	let dotEnv = require('dotenv').config().parsed

	console.log()
	if(! dotEnv['port']) {
		console.error(`.env file does not have port set, this won't work`)
		process.exit(1)
	}
	console.log('.env keys:')
	for (const k in dotEnv) {
		console.log(k)
	  }
	console.log()
  } catch (err) {
	console.error(err)
	console.error(`.env file does not exist, this won't work`)
	process.exit(1)
}

const express = require('express')
const cookieParser = require('cookie-parser')
const httpLogger = require('morgan')

// similar to https://expressjs.com/en/starter/generator.html except for folder structure
/**
 * Application class with epp property
 */
module.exports =  class EWApp { // express web app

/**
 * 
 * @returns AXlib version
 */
getAXtheVersion() { 
	return 'AX ver  0.8.0 '
}

eapp // express js app

_pugOptions = {// pug options
	basedir: './tmpl',
	pretty: true
}

constructor() {
	console.log(this.constructor.name,'node version:',JSON.stringify(process.versions.node))
	this.eapp = express()

	this.eapp.set('views', 'tmpl')
	this.eapp.set('view engine', 'pug')

	this.eapp.use(httpLogger('dev'))

	this.eapp.use(express.urlencoded({ extended: true }))
	this.eapp.use(express.json({ extended: true }))
	this.eapp.use(cookieParser())

	console.log(this.getAXtheVersion())
	this.eapp.get('/AXver', (req, res) => {
		res.send(this.getAXtheVersion())
	})
	
}//()

/**
 * Call at the end to configure the app
 */
finalPrep =(prt)=> {

	this.eapp.use(express.static('tmpl'))

	/* catch all for simple pug without args
	this.eapp.get('*', (req, res) =>{
		console.log('def handler', req.path) 
		if(!req.path.includes('.')) {
			res.setHeader('content-type', res.type('html'))   // => 'text/html'
			res.render(req.path.substring(1)+'index.pug', this._pugOptions )
		}
		else {
			console.error('not found ' + req.path)
			res.status(404).end()
		}
	})
	*/

	this.eapp.post('*', (req, res) =>{ 	// catch all for missed post
		console.error(this.constructor.name,'missed a POST', req.path.substring )
		res.status(404).end()
	})
	
	if(prt) console.error('passed port ignored, using .env file setting')

	this.eapp.listen(process.env.port) // listen on this port
	console.log(this.constructor.name, 'Serving:', process.cwd() + '/tmpl')
	console.log()
	console.log(this.constructor.name, 'WAPP ready, you can now open browser at:', process.env.port)
	console.log() //console.log(this.eapp._router.stack)

	this.eapp.use((err, req, res, next)=>{
		console.log('error:::', err)

		res.locals.message = err.message
		res.locals.error =  err

		res.status(err.status || 500)
		res.render('error')
	})
}//()

}//class
