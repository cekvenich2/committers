'use strict';

const multer  = require('multer') // for multipart|upload

/**
 * Before the page is shown
 */
module.exports =  class BasicPreRouter {// pug templating

	bmodel // stores reference to a model

	eapp

	// helps render
	_pugOptions = {// pug options
		basedir: './tmpl',
		pretty: true
	}

	pugOp(obj) { // get the pug options
		let obj2 = Object.assign(this._pugOptions, obj)
		return obj2
	}

	//call this insead of render like this:  
	/**
	 * @param {*} path 
	 * @param {*} res 
	 * @param {*} dat 
	 */
	rendr(path, res, dat) {
		res.setHeader('content-type', res.type('html'))   // => 'text/html'
		res.render(path, this.pugOp(dat))
	}
	
	constructor(eapp_) {
		this.eapp = eapp_
	}

	getPath(req) {
	  return './'+req.path+'/index'
	}

	getExpressApp() {
		return eapp
	}

}//class
