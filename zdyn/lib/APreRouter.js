const fs = require('fs-extra')

// ////////////////////////////////
const BasicPreRouter = require('axthe/BasicPreRouter.js')

const multer  = require('multer')

module.exports =  class APreRouter extends BasicPreRouter {// pug pre render. Most pages don't need a pre render, but some do

	constructor(eapp) {
		super(eapp)// call base and pass app, so it knows

		this.eapp.post('/api/form1', (req, res) => {
			console.log(this.constructor.name, req.body)
			res.send("OK") 
		})

		//this.bmodel.cloneFolder('mix', 'mix1')

		// /////////////////////////////////////////
		// http://maximorlov.com/fix-unexpected-field-error-multer
		// you should create  storage to pass to multer in {} but implement the storage in the Model, a bit like this: http://github.com/expressjs/multer/blob/master/StorageEngine.md 
		// and put validation there, like that is an image extension.
		// likely have storage save in user's folder under ./tmpl
		const upImg = multer({ }).single('img1')
		this.eapp.post('/upImg', (req, res) =>{
			upImg(req, res, function (err) {
				if (err ) {
					console.log(err)
				} else  {
					console.log(req.file)
					res.send('OK')
				}
			})
		})//()


		this.eapp.get('/api/video1',(req, res) => {
			// Ensure there is a range given for the video
			const range = req.headers.range;
			if (!range) {
			res.status(400).send("Requires Range header");
			}

			// get video stats (about 61MB)
			const videoPath = "./testi/bigbuck.mp4";
			const videoSize = fs.statSync(videoPath).size;

			// Parse Range
			// Example: "bytes=32324-"
			const CHUNK_SIZE = 10 ** 6; // 1MB
			const start = Number(range.replace(/\D/g, ""));
			const end = Math.min(start + CHUNK_SIZE, videoSize - 1);

			// Create headers
			const contentLength = end - start + 1;
			const headers = {
			"Content-Range": `bytes ${start}-${end}/${videoSize}`,
			"Accept-Ranges": "bytes",
			"Content-Length": contentLength,
			"Content-Type": "video/mp4",
			};

			// HTTP Status 206 for Partial Content
			res.writeHead(206, headers);

			// create video read stream for this particular chunk
			const videoStream = fs.createReadStream(videoPath, { start, end });

			// Stream the video chunk to the client
			videoStream.pipe(res);
		})
	
	}//()

}//class

