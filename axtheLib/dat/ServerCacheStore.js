
'use strict';

const DBl = require('./DBl')

/*
Like localStorage but on express server instance, great for caching; like REDIS or MemCache.
*/
module.exports = class ServerCacheStore {

	_hit = 0
	_miss = 0
	_db
	
	//  connect and create table if: not exists
	firstPrep() {
		// pass in amount of RAM to use for the store, default is 256 meg, else it goes to file
		this._db = new DBl()
		this._db.firstPrep('serverStore.litedb')
		if(this._db.tableExists('server_store'))
		return

		this._db.write(`
			CREATE TABLE server_store (
					key VARCHAR(256) PRIMARY KEY,
					val TEXT,
					linuxTime int
				);
			`)
		this._db.write(`CREATE UNIQUE INDEX kv ON server_store( key); `)
		}//()

	/**
	 * 
	 * @param {*} key Should be uniqe, could even be a GUID
	 * @param {*} val Sometimes a JSON string
	 */
	setItem(key, val, linuxTime){
		return this._db.write(`
			INSERT INTO server_store(key, val, linuxTime ) VALUES('${key}', '${val}', ${linuxTime})
				ON CONFLICT(key) DO 
					UPDATE SET val = '${val}', linuxTime = '${linuxTime}'
						WHERE key = '${key}'
			;
		`)
	}//()

	getItem(key){
		const ret= this._db.readOne(`
			SELECT val, linuxTime FROM server_store where key='${key}';
		`)
		
		if(ret) this._hit++
		else this._miss++

		return ret
	}//()

	removeItem(key){
		return this._db.write(`
			DELETE  FROM server_store where key='${key}';
		`)
	}//()


	//drops and recreate table
	clear() {
		this._db.write(`
			DROP TABLE server_store;
		`)

		// new
		this._db.write(`
			CREATE TABLE server_store (
					key VARCHAR(256) PRIMARY KEY,
					val TEXT,
					linuxTime int
				);
			`)
		this._db.write(`CREATE UNIQUE INDEX kv ON server_store( key, val, linuxTime); `)// yes, the answer is in index
	}//()


}
